package com.crossover.trial.weather.utils;

import com.crossover.trial.weather.exceptions.WeatherException;
import com.crossover.trial.weather.models.Airport;

public class AirportHelper {

	/** earth radius in KM */
	public static final double EARTH_RADIUS_IN_KM = 6372.8;

	/**
	 * Haversine distance between two airports.
	 *
	 * @param ad1
	 *            airport 1
	 * @param ad2
	 *            airport 2
	 * @return the distance in KM
	 * @throws WeatherException
	 */
	public static double calculateDistance(Airport ad1, Airport ad2) throws WeatherException {

		if (ad1 == null || ad2 == null) {
			throw new WeatherException("For calculateDistance() method airport objects can't be null!");
		}

		double deltaLat = Math.toRadians(ad2.getLatitude() - ad1.getLatitude());
		double deltaLon = Math.toRadians(ad2.getLongitude() - ad1.getLongitude());

		double a = Math.pow(Math.sin(deltaLat / 2), 2)
				+ Math.pow(Math.sin(deltaLon / 2), 2) * Math.cos(ad1.getLatitude()) * Math.cos(ad2.getLatitude());

		double c = 2 * Math.asin(Math.sqrt(a));
		return EARTH_RADIUS_IN_KM * c;
	}

	/*
	 * The latitude must be a number between -90 and 90
	 */
	public static double isLatitudeValid(String latitudeStr) throws WeatherException {

		double latitudeDouble = 0;
		boolean valid = false;
		try {

			latitudeDouble = Double.parseDouble(latitudeStr);

			if (latitudeDouble >= -90 && latitudeDouble <= 90) {
				valid = true;
			}
		} catch (Exception e) {
			// swallow
		}

		if (!valid) {
			throw new WeatherException("The latitude must be a number between -90 and 90! "
					+ "The given value of latitude: " + latitudeStr);
		}

		return latitudeDouble;
	}

	/*
	 * The longitude between -180 and 180.
	 */
	public static double isLongitudeValid(String longitudeStr) throws WeatherException {

		double longitudeDouble = 0;
		boolean valid = false;
		try {

			longitudeDouble = Double.parseDouble(longitudeStr);

			if (longitudeDouble >= -180 && longitudeDouble <= 180) {
				valid = true;
			}
		} catch (Exception e) {
			// swallow
		}

		if (!valid) {
			throw new WeatherException("The longitude must be a number between -180 and 180! "
					+ "The given value of longitude: " + longitudeStr);
		}

		return longitudeDouble;
	}

	/*
	 * IATA/FAA | 3-letter FAA code or IATA code (blank or "" if not assigned)
	 */
	public static boolean isIataFaaCodeValid(String iataFaaCodeStr) throws WeatherException {

		if (iataFaaCodeStr == null || !iataFaaCodeStr.matches("[A-Zs]+") || iataFaaCodeStr.length() != 3) {
			throw new WeatherException(
					"The IATA/FAA > 3-letter FAA code or IATA code must be 3-letter and capital letters only! "
							+ "The given value of  IATA/FAA: " + iataFaaCodeStr);
		}

		return true;
	}

	/*
	 * ICAO | 4-letter ICAO code (blank or "" if not assigned)
	 */
	public static boolean isIcaoCodeValid(String icaoCodeStr) throws WeatherException {

		if (icaoCodeStr == null || !icaoCodeStr.matches("[A-Z]+") || icaoCodeStr.length() != 4) {
			throw new WeatherException("The ICAO -> 4-letter ICAO code must be 4-letter and capital letters only! "
					+ "The given value of  ICAO: " + icaoCodeStr);
		}

		return true;
	}

	public static double isRadiusValid(String radiusStr) throws WeatherException {

		double radiusDouble = 0;
		boolean valid = false;
		try {

			radiusDouble = Double.parseDouble(radiusStr);

			if (radiusDouble >= 0 && radiusDouble <= EARTH_RADIUS_IN_KM) {
				valid = true;
			}
		} catch (Exception e) {
			// swallow
		}

		if (!valid) {
			throw new WeatherException("The radius in km  must be a number between 0 and " + EARTH_RADIUS_IN_KM + "! "
					+ "The given value of radius: " + radiusStr);
		}

		return radiusDouble;
	}

}
