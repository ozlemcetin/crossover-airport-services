package com.crossover.trial.weather.endpoints;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.crossover.trial.weather.exceptions.WeatherException;
import com.crossover.trial.weather.models.Airport;
import com.crossover.trial.weather.models.DataPoint;
import com.crossover.trial.weather.service.AirportService;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * A REST implementation of the WeatherCollector API. Accessible only to airport
 * weather collection sites via secure VPN.
 *
 * @author code test administrator
 */

@Path("/collect")
public class WeatherCollectorEndpointImp implements WeatherCollectorEndpoint {

	private final static Logger LOGGER = Logger.getLogger(WeatherCollectorEndpointImp.class.getName());

	/** shared gson json to object factory */
	private final static Gson gson = new Gson();

	// Service
	private AirportService airportService = AirportService.getInstance();

	public WeatherCollectorEndpointImp() {
		super();
	}

	// For Junit Testing
	public WeatherCollectorEndpointImp(AirportService airportService) {
		super();
		this.airportService = airportService;
	}

	@Override
	public Response ping() {

		try {

			return Response.status(Response.Status.OK).entity("ready").build();

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@Override
	public Response updateWeather(String iataCode, String pointType, String datapointJson) {

		try {

			// Update the AtmosphericInformation for the given Airport
			airportService.addDataPointForAirportByIataCode(iataCode, pointType,
					gson.fromJson(datapointJson, DataPoint.class));

			// Success
			return Response.status(Response.Status.OK).build();

		} catch (JsonSyntaxException | WeatherException e) {
			LOGGER.log(Level.INFO, e.getMessage(), e);
			return Response.status(422).build();

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@Override
	public Response getAirports() {

		try {

			Set<String> iatas = airportService.getIataListForAllAirports();
			return Response.status(Response.Status.OK).entity(iatas).build();

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@Override
	public Response getAirport(String iata) {

		try {

			// Find the airport
			Airport airport = airportService.findAirportByIataCode(iata);

			if (airport != null) {
				// Success
				return Response.status(Response.Status.OK).entity(airport).build();
			}

			// Failure
			return Response.status(Response.Status.NOT_FOUND).build();

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@Override
	public Response addAirport(String iata, String latString, String longString) {

		try {

			Airport newAirport = airportService.addAirportByIataCodeLatitudeLongitude(iata, latString, longString);

			if (newAirport != null) {
				// Success
				return Response.status(Response.Status.CREATED).build();
			}

			// Failure
			return Response.status(Response.Status.NO_CONTENT).build();

		} catch (JsonSyntaxException | WeatherException e) {
			LOGGER.log(Level.INFO, e.getMessage(), e);
			return Response.status(422).build();

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

	}

	@Override
	public Response deleteAirport(String iata) {

		try {

			boolean deleted = airportService.deleteAirportByIataCode(iata);

			if (deleted) {
				// Success
				return Response.status(Response.Status.OK).build();
			}

			// Failure
			return Response.status(Response.Status.NOT_FOUND).build();

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@Override
	public Response exit() {

		try {

			System.exit(0);
			return Response.noContent().build();

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

}
