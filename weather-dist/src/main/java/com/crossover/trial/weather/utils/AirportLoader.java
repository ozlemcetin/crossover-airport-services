package com.crossover.trial.weather.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.TimeZone;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.crossover.trial.weather.models.Airport;
import com.crossover.trial.weather.models.DaylightSavingsType;

/**
 * A simple airport loader which reads a file from disk and sends entries to the
 * webservice
 *
 * TODO: Implement the Airport Loader
 * 
 * @author code test administrator
 */
public class AirportLoader {

	private static final String BASE_URI = BaseUrl.BASE_URI_STR;

	/** end point to supply updates */
	private WebTarget collect;

	public AirportLoader() {
		Client client = ClientBuilder.newClient();
		collect = client.target(BASE_URI + "/collect");
	}

	public void upload(InputStream airportDataStream) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(airportDataStream));

		String line = null;
		while ((line = reader.readLine()) != null) {

			/*
			 * Line Example:
			 * 
			 * 0 1,
			 * 
			 * 1 "General Edward Lawrence Logan Intl",
			 * 
			 * 2 "Boston",
			 * 
			 * 3 "United States",
			 * 
			 * 4 "BOS",
			 * 
			 * 5 "KBOS",
			 * 
			 * 6 42.364347,
			 * 
			 * 7 -71.005181,
			 * 
			 * 8 19,
			 * 
			 * 9 -5,
			 * 
			 * 10 "A"
			 */

			String[] airportsAttributes = line.split(",");

			// Take off the " " signs
			String mainCityServedByAirport = airportsAttributes[2];
			if (airportsAttributes[2] != null && airportsAttributes[2].contains("\""))
				mainCityServedByAirport = airportsAttributes[2].substring(1, airportsAttributes[2].length() - 1);

			// Take off the " " signs
			String countryOrTerritoryAirportLocated = airportsAttributes[3];
			if (airportsAttributes[3] != null && airportsAttributes[3].contains("\""))
				countryOrTerritoryAirportLocated = airportsAttributes[3].substring(1,
						airportsAttributes[3].length() - 1);

			// Take off the " " signs
			String iataFaaCode3Letter = airportsAttributes[4];
			if (airportsAttributes[4] != null && airportsAttributes[4].contains("\""))
				iataFaaCode3Letter = airportsAttributes[4].substring(1, airportsAttributes[4].length() - 1);

			// Take off the " " signs
			String icaoCode4Letter = airportsAttributes[5];
			if (airportsAttributes[5] != null && airportsAttributes[5].contains("\""))
				icaoCode4Letter = airportsAttributes[5].substring(1, airportsAttributes[5].length() - 1);

			double latitude = Double.valueOf(airportsAttributes[6]);

			double longitude = Double.valueOf(airportsAttributes[7]);

			double altitude = Double.valueOf(airportsAttributes[8]);

			TimeZone timeZoneOffsetFromUTC = TimeZone.getTimeZone("GMT" + airportsAttributes[9]);

			// Take off the " " signs
			DaylightSavingsType dst = DaylightSavingsTypeHelper.getValue(airportsAttributes[10]);
			if (airportsAttributes[10] != null && airportsAttributes[10].contains("\""))
				dst = DaylightSavingsTypeHelper
						.getValue(airportsAttributes[10].substring(1, airportsAttributes[10].length() - 1));

			Airport airport = new Airport(mainCityServedByAirport, countryOrTerritoryAirportLocated, iataFaaCode3Letter,
					icaoCode4Letter, latitude, longitude, altitude, timeZoneOffsetFromUTC, dst);

			// @Path("/airport/{iata}/{lat}/{long}")
			WebTarget path = collect.path("/airport/" + airport.getIataFaaCode3Letter() + "/" + airport.getLatitude()
					+ "/" + airport.getLongitude());

			Response response = path.request().post(Entity.entity("", MediaType.TEXT_HTML_TYPE));
			System.out.println("collect.airport." + airport.getIataFaaCode3Letter() + "." + airport.getLatitude() + "."
					+ airport.getLongitude() + ": " + response.getStatusInfo());

		}
	}

	public static void main(String args[]) throws IOException {

		// Read data form airports.dat
		URL url = AirportLoader.class.getClassLoader().getResource("airports_1000.dat");

		AirportLoader al = new AirportLoader();
		al.upload(url.openStream());

		System.exit(0);
	}
}
