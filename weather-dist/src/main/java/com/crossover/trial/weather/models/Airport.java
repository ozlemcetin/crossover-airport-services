package com.crossover.trial.weather.models;

import java.util.Objects;
import java.util.TimeZone;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Basic airport information.
 *
 * @author code test administrator
 */
public class Airport {

	/*
	 * The airports.dat is a comma separated file with the following headers:
	 * 
	 */
	private final String VALUE_NOT_ASSIGNED = "NotAssigned";

	// City Main city served by airport. May be spelled differently from name.
	private String mainCityServedByAirport = VALUE_NOT_ASSIGNED;

	// Country Country or territory where airport is located.
	private String countryOrTerritoryAirportLocated = VALUE_NOT_ASSIGNED;

	// IATA/FAA 3-letter FAA code or IATA code (blank or "" if not assigned)
	/** the three letter IATA code */
	private String iataFaaCode3Letter = "";

	// ICAO 4-letter ICAO code (blank or "" if not assigned)
	private String icaoCode4Letter = "";

	/*
	 * Latitude Decimal degrees, up to 6 significant digits. Negative is South,
	 * positive is North.
	 */
	/** latitude value in degrees */
	private double latitude = 0.0;

	/*
	 * Longitude Decimal degrees, up to 6 significant digits. Negative is
	 * West,positive is East.
	 */
	/** longitude value in degrees */
	private double longitude = 0.0;

	// Altitude In feet
	private double altitude = 0.0;

	/*
	 * Timezone Hours offset from UTC. Fractional hours are expressed as decimals.
	 * (e.g. India is 5.5)
	 */
	private TimeZone timeZoneOffsetFromUTC = TimeZone.getDefault();

	/*
	 * DST One of E (Europe), A (US/Canada), S (South America), O (Australia), Z
	 * (New Zealand), N (None) or U (Unknown)
	 */
	private DaylightSavingsType dst = DaylightSavingsType.U;

	// Constructur using all fileds
	public Airport(String mainCityServedByAirport, String countryOrTerritoryAirportLocated, String iataFaaCode3Letter,
			String icaoCode4Letter, double latitude, double longitude, double altitude, TimeZone timeZoneOffsetFromUTC,
			DaylightSavingsType dst) {

		super();
		this.mainCityServedByAirport = mainCityServedByAirport;
		this.countryOrTerritoryAirportLocated = countryOrTerritoryAirportLocated;
		this.iataFaaCode3Letter = iataFaaCode3Letter;
		this.icaoCode4Letter = icaoCode4Letter;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.timeZoneOffsetFromUTC = timeZoneOffsetFromUTC;
		this.dst = dst;
	}

	public Airport(String iataCode, double latitude, double longitude) {

		this.iataFaaCode3Letter = iataCode;
		this.latitude = latitude;
		this.longitude = longitude;

	}

	// Only getter methods

	public String getMainCityServedByAirport() {
		return mainCityServedByAirport;
	}

	public String getCountryOrTerritoryAirportLocated() {
		return countryOrTerritoryAirportLocated;
	}

	public String getIataFaaCode3Letter() {
		return iataFaaCode3Letter;
	}

	public String getIcaoCode4Letter() {
		return icaoCode4Letter;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public TimeZone getTimeZoneOffsetFromUTC() {
		return timeZoneOffsetFromUTC;
	}

	public DaylightSavingsType getDst() {
		return dst;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
	}

	@Override
	public boolean equals(Object other) {

		if (other == null)
			return false;

		if (other == this)
			return true;

		if (other.getClass() != getClass())
			return false;

		Airport otherAirport = (Airport) other;
		return otherAirport.getIataFaaCode3Letter().equals(this.getIataFaaCode3Letter());
	}

	@Override
	public int hashCode() {

		return Objects.hash(this.getIataFaaCode3Letter());
	}

}
