package com.crossover.trial.weather;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.crossover.trial.weather.models.DataPoint;
import com.crossover.trial.weather.utils.BaseUrl;

/**
 * A reference implementation for the weather client. Consumers of the REST API
 * can look at WeatherClient to understand API semantics. This existing client
 * populates the REST endpoint with dummy data useful for testing.
 *
 * @author code test administrator
 */
public class WeatherClient {

	private static final String BASE_URI = BaseUrl.BASE_URI_STR;

	/** end point for read queries */
	private WebTarget query;

	/** end point to supply updates */
	private WebTarget collect;

	public WeatherClient() {
		Client client = ClientBuilder.newClient();
		query = client.target(BASE_URI + "/query");
		collect = client.target(BASE_URI + "/collect");
	}

	public void pingCollect() {

		WebTarget path = collect.path("/ping");

		Response response = path.request().get();
		System.out.println("collect.ping: " + response.readEntity(String.class));
	}

	public void updateWeather(String iata, String pointType, int first, int last, int mean, int median, int count) {

		WebTarget path = collect.path("/weather/" + iata + "/" + pointType);

		DataPoint dp = new DataPoint.Builder().withFirst(first).withLast(last).withMean(mean).withMedian(median)
				.withCount(count).build();
		Response response = path.request().post(Entity.entity(dp, "application/json"));
		System.out.println("collect.weather." + iata + "." + pointType + ": " + response.getStatusInfo());
	}

	public void getAllAirports() {

		WebTarget path = collect.path("/airports");

		Response response = path.request().get();
		System.out.println("collect.airports: " + response.getStatusInfo());
	}

	public void getAirport(String iata) {

		WebTarget path = collect.path("/airport/" + iata);

		Response response = path.request().get();
		System.out.println("collect.airport." + iata + ": " + response.getStatusInfo());
	}

	public void addAirport(String iata, String latString, String longString) {

		WebTarget path = collect.path("/airport/" + iata + "/" + latString + "/" + longString);

		Response response = path.request().post(Entity.entity("", MediaType.TEXT_HTML_TYPE));
		System.out.println(
				"collect.airport." + iata + "." + latString + "." + longString + ": " + response.getStatusInfo());
	}

	public void deleteAirport(String iata) {

		WebTarget path = collect.path("/airport/" + iata);

		Response response = path.request().delete();
		System.out.println("collect.airport." + iata + ": " + response.getStatusInfo());
	}

	public void pingQuery() {

		WebTarget path = query.path("/ping");

		Response response = path.request().get();
		System.out.println("query.ping: " + response.getStatusInfo());
	}

	public void weather(String iata, String radius) {

		WebTarget path = query.path("/weather/" + iata + "/" + radius);

		Response response = path.request().get();
		System.out.println("query.weather." + iata + ".radius: " + response.getStatusInfo());
	}

	public void exit() {
		try {
			collect.path("/exit").request().get();
		} catch (Throwable t) {
			// swallow
		}
	}

	public static void main(String[] args) {

		WeatherClient wc = new WeatherClient();

		wc.pingCollect();

		wc.addAirport("BOS", "42.364347", "-71.005181");
		wc.addAirport("EWR", "40.6925", "-74.168667");
		wc.addAirport("JFK", "40.639751", "-73.778925");
		wc.addAirport("LGA", "40.777245", "-73.872608");
		wc.addAirport("MMU", "40.79935", "-74.4148747");

		wc.getAllAirports();

		wc.getAirport("BOS");

		wc.updateWeather("BOS", "WIND", 0, 10, 6, 4, 20);

		wc.deleteAirport("BOS");

		// wc.weather("BOS", "0");
		wc.weather("JFK", "0");
		wc.weather("EWR", "0");
		wc.weather("LGA", "0");
		wc.weather("MMU", "0");

		wc.pingQuery();

		wc.exit();

		System.out.print("complete");
		System.exit(0);
	}
}
