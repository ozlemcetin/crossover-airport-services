package com.crossover.trial.weather.models;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * A collected point, including some information about the range of collected
 * values
 *
 * @author code test administrator
 */
public class DataPoint {

	private double mean = 0.0;
	private int first = 0;
	private int second = 0;
	private int third = 0;
	private int count = 0;

	protected DataPoint(double mean, int first, int second, int third, int count) {
		this.setMean(mean);
		this.setFirst(first);
		this.setSecond(second);
		this.setThird(third);
		this.setCount(count);
	}

	public DataPoint(Builder builder) {
		this.setMean(builder.mean);
		this.setFirst(builder.first);
		this.setSecond(builder.second);
		this.setThird(builder.third);
		this.setCount(builder.count);
	}

	/** the mean of the observations */
	public double getMean() {
		return mean;
	}

	protected void setMean(double mean) {
		this.mean = mean;
	}

	/** 1st quartile -- useful as a lower bound */
	public int getFirst() {
		return first;
	}

	protected void setFirst(int first) {
		this.first = first;
	}

	/** 2nd quartile -- median value */
	public int getSecond() {
		return second;
	}

	protected void setSecond(int second) {
		this.second = second;
	}

	/** 3rd quartile value -- less noisy upper value */
	public int getThird() {
		return third;
	}

	protected void setThird(int third) {
		this.third = third;
	}

	/** the total number of measurements */
	public int getCount() {
		return count;
	}

	protected void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
	}

	@Override
	public boolean equals(Object that) {
		return this.toString().equals(that.toString());
	}

	static public class Builder {

		private double mean = 0.0;
		private int first = 0;
		private int second = 0;
		private int third = 0;
		private int count = 0;

		public Builder() {
		}

		public Builder withMean(double mean) {
			this.mean = mean;
			return this;
		}

		public Builder withFirst(int first) {
			this.first = first;
			return this;
		}

		// second
		public Builder withMedian(int median) {
			this.second = median;
			return this;
		}

		// third
		public Builder withLast(int last) {
			this.third = last;
			return this;
		}

		public Builder withCount(int count) {
			this.count = count;
			return this;
		}

		public DataPoint build() {
			return new DataPoint(this);
		}
	}
}
