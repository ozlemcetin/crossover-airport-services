package com.crossover.trial.weather.utils;

import com.crossover.trial.weather.models.DaylightSavingsType;

public class DaylightSavingsTypeHelper {

	public static DaylightSavingsType getValue(String dstStr) {

		if (DaylightSavingsType.E.toString().equals(dstStr)) {
			return DaylightSavingsType.E;

		} else if (DaylightSavingsType.A.toString().equals(dstStr)) {
			return DaylightSavingsType.A;

		} else if (DaylightSavingsType.S.toString().equals(dstStr)) {
			return DaylightSavingsType.S;

		} else if (DaylightSavingsType.O.toString().equals(dstStr)) {
			return DaylightSavingsType.O;

		} else if (DaylightSavingsType.Z.toString().equals(dstStr)) {
			return DaylightSavingsType.Z;

		} else if (DaylightSavingsType.N.toString().equals(dstStr)) {
			return DaylightSavingsType.N;

		} else if (DaylightSavingsType.U.toString().equals(dstStr)) {
			return DaylightSavingsType.U;

		} else
			throw new IllegalArgumentException();
	}

}
