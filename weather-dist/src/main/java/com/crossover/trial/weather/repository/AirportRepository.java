package com.crossover.trial.weather.repository;

import java.util.Collection;
import java.util.Set;

import com.crossover.trial.weather.models.Airport;
import com.crossover.trial.weather.models.AtmosphericInformation;

public interface AirportRepository {

	/*
	 * Methods
	 */

	public Set<String> getIataListForAllAirports();

	public Collection<Airport> getAllAirportList();

	public Collection<AtmosphericInformation> getAtmosphericInformationListForAllAirports();

	public boolean airportExists(String iataFaaCode);

	public Airport findAirportByIataCode(String iataFaaCode);

	public void deleteAirport(Airport airport);

	public void addAirport(Airport newAirport);

	public boolean atmosphericInformationExistsForAirportIata(String iataFaaCode);

	public AtmosphericInformation findAtmosphericInformationForAirportIata(String iataFaaCode);

	public void updateAtmosphericInformationForAirportIata(String iataFaaCode, AtmosphericInformation ai);

	public void updateRequestFrequencyForAirportIata(String iataFaaCode);

	public Integer getRequestFrequencyForAirportIata(String iataFaaCode);

	public int getRequestFrequencySize();

	public void updateRadiusFrequencyForRadius(Double radius);

	public int[] histArrayForRadiusFrequencyEntries();
}
