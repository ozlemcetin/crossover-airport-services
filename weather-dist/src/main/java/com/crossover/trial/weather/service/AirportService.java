package com.crossover.trial.weather.service;

import java.util.Collection;
import java.util.Set;

import com.crossover.trial.weather.exceptions.WeatherException;
import com.crossover.trial.weather.models.Airport;
import com.crossover.trial.weather.models.AtmosphericInformation;
import com.crossover.trial.weather.models.DataPoint;
import com.crossover.trial.weather.models.DataPointType;
import com.crossover.trial.weather.repository.AirportRepository;
import com.crossover.trial.weather.repository.AirportRepositoryImp;
import com.crossover.trial.weather.utils.AirportHelper;
import com.crossover.trial.weather.utils.DataPointHelper;

public class AirportService {

	// Singleton
	private static AirportService singleton = new AirportService(AirportRepositoryImp.getInstance());

	// Repository
	private AirportRepository airportRepository;

	// For Junit Testing
	public AirportService(AirportRepository airportRepository) {
		super();
		this.airportRepository = airportRepository;
	}

	// For Endpoints Imp.
	public static AirportService getInstance() {
		return singleton;
	}

	/*
	 * Methods
	 */

	public Set<String> getIataListForAllAirports() {
		return airportRepository.getIataListForAllAirports();
	}

	public Collection<Airport> getAllAirportList() {

		return airportRepository.getAllAirportList();
	}

	public Collection<AtmosphericInformation> getAtmosphericInformationListForAllAirports() {

		return airportRepository.getAtmosphericInformationListForAllAirports();
	}

	public Airport findAirportByIataCode(String iataFaaCode) {

		if (airportRepository.airportExists(iataFaaCode))
			return airportRepository.findAirportByIataCode(iataFaaCode);

		return null;
	}

	public boolean deleteAirportByIataCode(String iataFaaCode) {

		// Find airport
		Airport airport = airportRepository.findAirportByIataCode(iataFaaCode);

		if (airport != null) {
			// Delete the airport
			airportRepository.deleteAirport(airport);
			return true;
		}

		return false;
	}

	/**
	 * Add a new known airport to our list.
	 *
	 * @param iataCode
	 *            3 letter code
	 * @param latitude
	 *            in degrees
	 * @param longitude
	 *            in degrees
	 *
	 * @return the added airport
	 */
	public Airport addAirportByIataCodeLatitudeLongitude(String iataFaaCode, String latitude, String longitude)
			throws WeatherException {

		/*
		 * If the repository doesn't contains the airport iataCode yet, adding will be
		 * succesful
		 */
		if (airportRepository.airportExists(iataFaaCode))
			throw new WeatherException(iataFaaCode
					+ " iata code of an Airport already exists, a new airport cannot be added with the already used airport iata code!");

		// isIataFaaCodeValid?
		AirportHelper.isIataFaaCodeValid(iataFaaCode);

		// isLatitudeValid?
		double latitudeDouble = AirportHelper.isLatitudeValid(latitude);

		// isLongitudeValid?
		double longitudeDouble = AirportHelper.isLongitudeValid(longitude);

		// Add New Airport
		Airport newAirport = new Airport(iataFaaCode, latitudeDouble, longitudeDouble);
		airportRepository.addAirport(newAirport);

		return newAirport;

	}

	/**
	 * Update the airports weather data with the collected data.
	 *
	 * @param iataCode
	 *            the 3 letter IATA code
	 * @param pointType
	 *            the point type {@link DataPointType}
	 * @param dp
	 *            a datapoint object holding pointType data
	 *
	 * @throws WeatherException
	 *             if the update can not be completed
	 */

	public boolean addDataPointForAirportByIataCode(String iataFaaCode, String pointType, DataPoint dp)
			throws WeatherException {

		/*
		 * If the repository doesn't contains the airport iataCode yet, adding a data
		 * point cannot be done
		 */
		if (!airportRepository.airportExists(iataFaaCode))
			throw new WeatherException(iataFaaCode
					+ " iata code of an Airport doesn't exist, a new data point cannot be added for a non-existing airport!");

		// Find AtmosphericInformation for Airport
		AtmosphericInformation ai = airportRepository.findAtmosphericInformationForAirportIata(iataFaaCode);

		if (ai == null) {
			// Create an empty AtmosphericInformation value
			ai = new AtmosphericInformation();
		}

		// Change the AtmosphericInformation
		{
			// Find a Data Point Type for the pointType of String
			DataPointType dptype = DataPointHelper.getDataPointForThePointTypeStr(pointType);

			// Is Data Point valid for its type?
			DataPointHelper.isDataPointValidForItsType(dptype, dp);

			// Set DataPoint for AtmosphericInformation
			ai.addDataPointForItsTypeInAtmosphericInformation(dptype, dp);
		}

		// Update the AtmosphericInformation for the given Airport
		airportRepository.updateAtmosphericInformationForAirportIata(iataFaaCode, ai);

		return true;
	}

	public AtmosphericInformation getAtmosphericInformationForAirport(String iataFaaCode) {

		// Find AtmosphericInformation for Airport
		return airportRepository.findAtmosphericInformationForAirportIata(iataFaaCode);

	}

	/**
	 * Records information about how often requests are made
	 *
	 * @param iata
	 *            an iata code
	 * @param radius
	 *            query radius
	 */
	public boolean updateRequestAndRadiusFrequencyForAirportByIataCode(String iataFaaCode, double radiusDouble)
			throws WeatherException {

		/*
		 * If the repository doesn't contains the airport iataCode yet, updating info
		 * cannot be done
		 */
		if (!airportRepository.airportExists(iataFaaCode))
			throw new WeatherException(iataFaaCode
					+ " iata code of an Airport doesn't exist, request and radius frequencies cannot be updated for a non-existing airport!");

		// Update the requestFrequency for the given Airport
		airportRepository.updateRequestFrequencyForAirportIata(iataFaaCode);

		// Update the radiusFrequency
		airportRepository.updateRadiusFrequencyForRadius(radiusDouble);

		return true;

	}

	public double getRequestFrequencyRatioFracForAirport(Airport airport) throws WeatherException {

		/*
		 * The airport is invalid
		 */
		if (airport == null)
			throw new WeatherException("Request Frequency Ratio cannot be read for the invalid airport : " + airport);

		/*
		 * If the repository doesn't contains the airport iataCode yet, updating info
		 * cannot be done
		 */
		if (!airportRepository.airportExists(airport.getIataFaaCode3Letter()))
			throw new WeatherException(airport.getIataFaaCode3Letter()
					+ " iata code of an Airport doesn't exist, request frequency ratio cannot be read for a non-existing airport!");

		// Get the freq and find the ratio
		Integer freq = airportRepository.getRequestFrequencyForAirportIata(airport.getIataFaaCode3Letter());
		int size = airportRepository.getRequestFrequencySize();

		if (size <= 0)
			throw new WeatherException(
					"Request frequency ratio cannot be read. Request frequency  size is equals to or less than zero (0) for the given airport of : "
							+ airport.getIataFaaCode3Letter());

		double frac = (double) freq / size;
		return frac;

	}

	public int[] histForRadiusFrequency() {

		return airportRepository.histArrayForRadiusFrequencyEntries();
	}

}
