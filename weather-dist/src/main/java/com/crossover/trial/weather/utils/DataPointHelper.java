package com.crossover.trial.weather.utils;

import com.crossover.trial.weather.exceptions.WeatherException;
import com.crossover.trial.weather.models.DataPoint;
import com.crossover.trial.weather.models.DataPointType;

public class DataPointHelper {

	public static DataPointType getDataPointForThePointTypeStr(String pointType) throws WeatherException {

		DataPointType dptype = null;
		try {

			// Find a Data Point Type for the pointType of String
			dptype = DataPointType.valueOf(pointType.toUpperCase());

		} catch (Exception e) {
			throw new WeatherException("PointType of " + pointType + " is not a valid Data Point Type value!");
		}

		return dptype;
	}

	public static boolean isDataPointValidForItsType(DataPointType dptype, DataPoint dp) throws WeatherException {

		if (dptype == DataPointType.WIND) {

			if (dp.getMean() >= 0) {
				return true;

			} else {
				throw new WeatherException("For data point type of " + dptype.toString()
						+ " mean must be bigger than/equals to 0!" + " The given mean value :" + dp.getMean());
			}

		} else if (dptype == DataPointType.TEMPERATURE) {

			if (dp.getMean() >= -50 && dp.getMean() < 100) {
				return true;

			} else {
				throw new WeatherException("For data point type of " + dptype.toString()
						+ " mean must be bigger than/equals to -50 and less than 100!" + " The given mean value :"
						+ dp.getMean());
			}

		} else if (dptype == DataPointType.PRESSURE) {

			if (dp.getMean() >= 650 && dp.getMean() < 800) {
				return true;

			} else {
				throw new WeatherException("For data point type of " + dptype.toString()
						+ " mean must be bigger than/equals to 650 and less than 800!" + " The given mean value :"
						+ dp.getMean());
			}

		} else if (dptype == DataPointType.HUMIDTY || dptype == DataPointType.CLOUDCOVER
				|| dptype == DataPointType.PRECIPITATION) {

			if (dp.getMean() >= 0 && dp.getMean() < 100) {
				return true;

			} else {
				throw new WeatherException("For data point type of " + dptype.toString()
						+ " mean must be bigger than/equals to 0 and less than 100!" + " The given mean value :"
						+ dp.getMean());
			}

		}

		return false;
	}
}
