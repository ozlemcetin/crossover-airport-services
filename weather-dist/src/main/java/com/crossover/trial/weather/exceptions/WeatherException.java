package com.crossover.trial.weather.exceptions;

/**
 * An internal exception marker
 */
public class WeatherException extends Exception {

	public WeatherException(String message) {
		super(message);
	}

	public WeatherException(String message, Throwable cause) {
		super(message, cause);

	}
}
