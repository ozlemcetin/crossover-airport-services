package com.crossover.trial.weather.models;

/*
 *  Daylight Savings Time: One of 
 *  	E (Europe), 
 *  	A (US/Canada),
		S (South America), 
		O (Australia), 
		Z (New Zealand), 
		N (None) or
 		U (Unknown)	
 */

public enum DaylightSavingsType {

	E("Europe"), A("US/Canada"), S("South America"), O("Australia"), Z("New Zealand"), N("None"), U("Unknown");

	private final String continent;

	private DaylightSavingsType(String continent) {
		this.continent = continent;
	}

	public String getContinent() {
		return continent;
	}

}
