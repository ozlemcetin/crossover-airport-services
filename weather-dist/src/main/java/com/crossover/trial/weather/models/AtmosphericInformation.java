package com.crossover.trial.weather.models;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * encapsulates sensor information for a particular location
 */
public class AtmosphericInformation {

	/*
	 * Instead of having attributes lists like following, use data point and data
	 * point type map.
	 */

	/** wind speed in km/h */
	// private DataPoint wind;

	/** temperature in degrees celsius */
	// private DataPoint temperature;

	/** humidity in percent */
	// private DataPoint humidity;

	/** pressure in mmHg */
	// private DataPoint pressure;

	/** cloud cover percent from 0 - 100 (integer) */
	// private DataPoint cloudCover;

	/** precipitation in cm */
	// private DataPoint precipitation;

	/** the last time this data was updated, in milliseconds since UTC epoch */
	private long lastUpdateTime;

	Map<DataPointType, DataPoint> maps;

	/*
	 * Constructor
	 */
	public AtmosphericInformation() {

		this.lastUpdateTime = System.currentTimeMillis();
		maps = new ConcurrentHashMap<DataPointType, DataPoint>();
	}

	/*
	 * Add & Get Elements form the Map
	 */
	public void addDataPointForItsTypeInAtmosphericInformation(DataPointType dptype, DataPoint dp) {

		this.lastUpdateTime = System.currentTimeMillis();
		maps.put(dptype, dp);
	}

	public DataPoint findDataPointForItsTypeInAtmosphericInformation(DataPointType dptype) {

		return maps.get(dptype);
	}

	public int getDataPointMapSize() {
		return maps.size();
	}

	/*
	 * Getter
	 */

	public long getLastUpdateTime() {
		return lastUpdateTime;
	}

}
