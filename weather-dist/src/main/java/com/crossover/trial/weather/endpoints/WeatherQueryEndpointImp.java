package com.crossover.trial.weather.endpoints;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.crossover.trial.weather.exceptions.WeatherException;
import com.crossover.trial.weather.models.Airport;
import com.crossover.trial.weather.models.AtmosphericInformation;
import com.crossover.trial.weather.service.AirportService;
import com.crossover.trial.weather.utils.AirportHelper;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * The Weather App REST endpoint allows clients to query, update and check
 * health stats. Currently, all data is held in memory. The end point deploys to
 * a single container
 *
 * @author code test administrator
 */

@Path("/query")
public class WeatherQueryEndpointImp implements WeatherQueryEndpoint {

	private final static Logger LOGGER = Logger.getLogger(WeatherQueryEndpointImp.class.getName());

	/** shared gson json to object factory */
	private final static Gson gson = new Gson();

	// Service
	private AirportService airportService = AirportService.getInstance();

	public WeatherQueryEndpointImp() {
		super();
	}

	// For Junit Testing
	public WeatherQueryEndpointImp(AirportService airportService) {
		super();
		this.airportService = airportService;
	}

	/**
	 * Retrieve service health including total size of valid data points and request
	 * frequency information.
	 *
	 * @return health stats for the service as a string
	 */
	@Override
	public String ping() {

		try {

			Map<String, Object> retval = new HashMap<>();

			// Read all AtmosphericInformation for all Airports
			Collection<AtmosphericInformation> ais = airportService.getAtmosphericInformationListForAllAirports();

			int datasize = 0;
			for (AtmosphericInformation ai : ais) {

				// we only count recent readings
				if (ai != null && ai.getDataPointMapSize() > 0) {

					// updated in the last day
					if (ai.getLastUpdateTime() > System.currentTimeMillis() - 86400000) {
						datasize++;
					}
				}
			}

			// datasize info
			retval.put("datasize", datasize);

			// Read all Airports
			Collection<Airport> airports = airportService.getAllAirportList();
			Map<String, Double> freq = new HashMap<>();

			// fraction of queries
			for (Airport ap : airports) {

				try {
					// For each airport
					double frac = airportService.getRequestFrequencyRatioFracForAirport(ap);
					freq.put(ap.getIataFaaCode3Letter(), frac);

				} catch (WeatherException e) {
					LOGGER.log(Level.INFO, e.getMessage(), e);
				}
			}

			// iata_freq info
			retval.put("iata_freq", freq);

			// Radious freq info
			int[] hist = airportService.histForRadiusFrequency();
			retval.put("radius_freq", hist);

			return gson.toJson(retval);

		} catch (JsonSyntaxException e) {
			LOGGER.log(Level.INFO, e.getMessage(), e);
			return null;

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			return null;
		}
	}

	/**
	 * Given a query in json format {'iata': CODE, 'radius': km} extracts the
	 * requested airport information and return a list of matching atmosphere
	 * information.
	 *
	 * @param iata
	 *            the iataCode
	 * @param radiusString
	 *            the radius in km
	 *
	 * @return a list of atmospheric information
	 */
	@Override
	public Response weather(String iata, String radiusString) {

		try {

			// isRadiusValid?
			double radiusDouble = AirportHelper.isRadiusValid(radiusString);

			// updateRequestAndRadiusFrequencyForAirportByIataCode
			airportService.updateRequestAndRadiusFrequencyForAirportByIataCode(iata, radiusDouble);

			// Find the airport
			Airport airport = airportService.findAirportByIataCode(iata);

			List<AtmosphericInformation> retval = new ArrayList<>();
			if (radiusDouble <= 0) {

				AtmosphericInformation ai = airportService.getAtmosphericInformationForAirport(iata);
				retval.add(ai);

			} else {

				// Find all airports
				Collection<Airport> airports = airportService.getAllAirportList();

				for (Airport ap : airports) {

					if (AirportHelper.calculateDistance(airport, ap) <= radiusDouble) {

						AtmosphericInformation ai = airportService
								.getAtmosphericInformationForAirport(ap.getIataFaaCode3Letter());

						if (ai != null && ai.getDataPointMapSize() > 0) {
							retval.add(ai);
						}
					}
				} // For Loop
			} // Radious bigger than zero

			// Success
			return Response.status(Response.Status.OK).entity(retval).build();

		} catch (WeatherException e) {
			LOGGER.log(Level.INFO, e.getMessage(), e);
			return Response.status(422).build();

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

}
