package com.crossover.trial.weather.repository;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.crossover.trial.weather.models.Airport;
import com.crossover.trial.weather.models.AtmosphericInformation;

public class AirportRepositoryImp implements AirportRepository {

	// Singleton
	private static final AirportRepositoryImp singleton = new AirportRepositoryImp();

	/*
	 * Airport data in here
	 */
	private Map<String, Airport> airports;

	/**
	 * Each Airport key (iataFaaCode3Letter of an Airport ), has an
	 * AtmosphericInformation value.
	 */
	private Map<String, AtmosphericInformation> atmosInfosForAirports;

	/**
	 * Internal performance counter to better understand most requested information,
	 * this map can be improved but for now provides the basis for future
	 * performance optimizations. Due to the stateless deployment architecture we
	 * don't want to write this to disk, but will pull it off using a REST request
	 * and aggregate with other performance metrics {@link #ping()}
	 */
	private Map<String, Integer> requestFrequency;

	private Map<Double, Integer> radiusFrequency;

	/*
	 * Constructors
	 */

	// For Junit Testing
	public AirportRepositoryImp() {

		airports = new ConcurrentHashMap<String, Airport>();
		atmosInfosForAirports = new ConcurrentHashMap<String, AtmosphericInformation>();
		requestFrequency = new ConcurrentHashMap<String, Integer>();
		radiusFrequency = new ConcurrentHashMap<Double, Integer>();
	}

	// For Endpoints Imp.
	public static AirportRepositoryImp getInstance() {
		return singleton;
	}

	/*
	 * Methods
	 */

	@Override
	public Set<String> getIataListForAllAirports() {

		// Key set
		return airports.keySet();
	}

	@Override
	public Collection<Airport> getAllAirportList() {

		// Value set
		return airports.values();
	}

	@Override
	public Collection<AtmosphericInformation> getAtmosphericInformationListForAllAirports() {

		// Value set
		return atmosInfosForAirports.values();
	}

	@Override
	public boolean airportExists(String iataFaaCode) {
		return airports.containsKey(iataFaaCode);
	}

	@Override
	public Airport findAirportByIataCode(String iataFaaCode) {

		return airports.get(iataFaaCode);
	}

	@Override
	public void deleteAirport(Airport airport) {

		// Remove the airport
		airports.remove(airport.getIataFaaCode3Letter());
	}

	@Override
	public void addAirport(Airport newAirport) {

		// Add the airport
		airports.put(newAirport.getIataFaaCode3Letter(), newAirport);
	}

	@Override
	public boolean atmosphericInformationExistsForAirportIata(String iataFaaCode) {
		return atmosInfosForAirports.containsKey(iataFaaCode);
	}

	@Override
	public AtmosphericInformation findAtmosphericInformationForAirportIata(String iataFaaCode) {

		return atmosInfosForAirports.get(iataFaaCode);
	}

	@Override
	public void updateAtmosphericInformationForAirportIata(String iataFaaCode, AtmosphericInformation ai) {

		// Update the airport with given AtmosphericInformation value
		atmosInfosForAirports.put(iataFaaCode, ai);
	}

	@Override
	public void updateRequestFrequencyForAirportIata(String iataFaaCode) {

		requestFrequency.put(iataFaaCode, requestFrequency.getOrDefault(iataFaaCode, 0) + 1);
	}

	@Override
	public Integer getRequestFrequencyForAirportIata(String iataFaaCode) {

		return requestFrequency.getOrDefault(iataFaaCode, 0);
	}

	@Override
	public int getRequestFrequencySize() {

		return requestFrequency.size();
	}

	@Override
	public void updateRadiusFrequencyForRadius(Double radius) {

		radiusFrequency.put(radius, radiusFrequency.getOrDefault(radius, 0));
	}

	@Override
	public int[] histArrayForRadiusFrequencyEntries() {

		int m = radiusFrequency.keySet().stream().max(Double::compare).orElse(1000.0).intValue() + 1;
		int[] hist = new int[m];

		for (Map.Entry<Double, Integer> e : radiusFrequency.entrySet()) {

			int i = e.getKey().intValue() % 10;
			hist[i] += e.getValue();
		}

		return hist;
	}

}
