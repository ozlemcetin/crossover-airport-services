package com.crossover.trial.weather.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.crossover.trial.weather.exceptions.WeatherException;
import com.crossover.trial.weather.models.Airport;
import com.crossover.trial.weather.models.AtmosphericInformation;
import com.crossover.trial.weather.models.DataPoint;
import com.crossover.trial.weather.models.DataPointType;
import com.crossover.trial.weather.repository.AirportRepositoryImp;

public class AirportServiceTest {

	// Service
	private AirportService airportService;

	// Airports
	private Airport airportBOS;
	private Airport airportEWR;
	private Airport airportJFK;
	private Airport airportLGA;
	private Airport airportMMU;

	// Data Point
	private DataPoint dataPoint;

	// Point Type
	private String windPointType;

	// Radius
	private Double radius;

	@Before
	public void setUp() throws Exception {

		// Radius
		radius = new Double(200);

		// Data Point Type
		windPointType = DataPointType.WIND.toString();

		// Data Point
		dataPoint = new DataPoint.Builder().withCount(10).withFirst(10).withMedian(20).withLast(30).withMean(22)
				.build();

		// Service - AirportRepositoryImp
		airportService = new AirportService(new AirportRepositoryImp());

		// Airports
		airportBOS = new Airport("BOS", 42.364347, -71.005181);
		airportService.addAirportByIataCodeLatitudeLongitude(airportBOS.getIataFaaCode3Letter(),
				Double.toString(airportBOS.getLatitude()), Double.toString(airportBOS.getLongitude()));

		airportEWR = new Airport("EWR", 40.6925, -74.168667);
		airportService.addAirportByIataCodeLatitudeLongitude(airportEWR.getIataFaaCode3Letter(),
				Double.toString(airportEWR.getLatitude()), Double.toString(airportEWR.getLongitude()));

		airportJFK = new Airport("JFK", 40.639751, -73.778925);
		airportService.addAirportByIataCodeLatitudeLongitude(airportJFK.getIataFaaCode3Letter(),
				Double.toString(airportJFK.getLatitude()), Double.toString(airportJFK.getLongitude()));

		airportLGA = new Airport("LGA", 40.777245, -73.872608);
		airportService.addAirportByIataCodeLatitudeLongitude(airportLGA.getIataFaaCode3Letter(),
				Double.toString(airportLGA.getLatitude()), Double.toString(airportLGA.getLongitude()));

		airportMMU = new Airport("MMU", 40.79935, -74.4148747);
		airportService.addAirportByIataCodeLatitudeLongitude(airportMMU.getIataFaaCode3Letter(),
				Double.toString(airportMMU.getLatitude()), Double.toString(airportMMU.getLongitude()));

	}

	@Test
	public void test_getIataListForAllAirports() throws Exception {

		Set<String> iatas = airportService.getIataListForAllAirports();

		assertEquals(5, iatas.size());
		assertTrue(iatas.contains(airportBOS.getIataFaaCode3Letter()));
		assertTrue(iatas.contains(airportEWR.getIataFaaCode3Letter()));
		assertTrue(iatas.contains(airportJFK.getIataFaaCode3Letter()));
		assertTrue(iatas.contains(airportLGA.getIataFaaCode3Letter()));
		assertTrue(iatas.contains(airportMMU.getIataFaaCode3Letter()));
	}

	@Test
	public void test_getAllAirportList() throws Exception {

		Collection<Airport> airports = airportService.getAllAirportList();

		assertEquals(5, airports.size());
		assertTrue(airports.contains(airportBOS));
		assertTrue(airports.contains(airportEWR));
		assertTrue(airports.contains(airportJFK));
		assertTrue(airports.contains(airportLGA));
		assertTrue(airports.contains(airportMMU));
	}

	@Test
	public void test_getAtmosphericInformationListForAllAirports() throws Exception {

		Collection<AtmosphericInformation> ais = airportService.getAtmosphericInformationListForAllAirports();
		assertEquals(0, ais.size());
	}

	@Test

	public void test_findAirportByIataCode() throws Exception {

		// Valid Airport
		Airport ap = airportService.findAirportByIataCode(airportBOS.getIataFaaCode3Letter());
		assertEquals(ap, airportBOS);

		// Invalid Airport
		Airport ap2 = airportService.findAirportByIataCode("CRS");
		assertNull(ap2);
	}

	@Test
	public void test_deleteAirportByIataCode() throws Exception {

		// Valid Airport
		boolean deleted = airportService.deleteAirportByIataCode(airportBOS.getIataFaaCode3Letter());
		assertTrue(deleted);

		// Invalid Airport
		deleted = airportService.deleteAirportByIataCode("CRS");
		assertFalse(deleted);
	}

	@Test
	public void test_addAirportByIataCodeLatitudeLongitude() throws Exception {

		try {
			// Already exists
			airportService.addAirportByIataCodeLatitudeLongitude(airportBOS.getIataFaaCode3Letter(), "42.364347",
					"-71.005181");

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		try {
			// Invalid IATA code
			airportService.addAirportByIataCodeLatitudeLongitude("CROSOVER", "42.364347", "-71.005181");

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		try {
			// Invalid Latitude
			airportService.addAirportByIataCodeLatitudeLongitude("CRS", "200.0", "-71.005181");

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		try {
			// Invalid Longtitude
			airportService.addAirportByIataCodeLatitudeLongitude("CRS", "42.364347", "200.0");

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		// Added
		Airport airportAdded = airportService.addAirportByIataCodeLatitudeLongitude("CRT", "42.364347",
				"-71.005181");
		assertNotNull(airportAdded);

	}

	@Test
	public void test_addDataPointForAirportByIataCode() throws Exception {

		try {
			// Invalid IATA Code
			airportService.addDataPointForAirportByIataCode("cb?", windPointType, dataPoint);

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		try {
			// Invalid Airport
			airportService.addDataPointForAirportByIataCode("CRS", windPointType, dataPoint);

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		try {
			// Invalid Point Type
			airportService.addDataPointForAirportByIataCode(airportBOS.getIataFaaCode3Letter(), "CROSOVERPOINTTYPE",
					dataPoint);

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		try {
			// Invalid Data Point
			DataPoint dp = new DataPoint.Builder().withMean(-200).build();
			airportService.addDataPointForAirportByIataCode(airportBOS.getIataFaaCode3Letter(), windPointType, dp);

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		boolean addded = airportService.addDataPointForAirportByIataCode(airportBOS.getIataFaaCode3Letter(),
				windPointType, dataPoint);
		assertTrue(addded);

	}

	@Test
	public void test_getAtmosphericInformationForAirport() throws Exception {

		AtmosphericInformation ai = airportService
				.getAtmosphericInformationForAirport(airportBOS.getIataFaaCode3Letter());
		assertNull(ai);
	}

	@Test
	public void test_updateRequestAndRadiusFrequencyForAirportByIataCode() throws Exception {

		try {
			// Invalid Airport
			airportService.updateRequestAndRadiusFrequencyForAirportByIataCode("CRS", radius);

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		// Valid Airport
		boolean updated = airportService
				.updateRequestAndRadiusFrequencyForAirportByIataCode(airportBOS.getIataFaaCode3Letter(), radius);
		assertTrue(updated);

	}

	@Test
	public void test_getRequestFrequencyRatioFracForAirport() throws Exception {

		try {
			// Null
			airportService.getRequestFrequencyRatioFracForAirport(null);

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		try {
			// Invalid airport
			Airport airport = new Airport("CRS", 42.364347, -71.005181);
			airportService.getRequestFrequencyRatioFracForAirport(airport);

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		try {
			// Frequncy size 0
			airportService.getRequestFrequencyRatioFracForAirport(airportBOS);

		} catch (WeatherException e) {
			assertNotNull(e);
		}

		// updateRequestAndRadiusFrequencyForAirportByIataCode
		airportService.updateRequestAndRadiusFrequencyForAirportByIataCode(airportBOS.getIataFaaCode3Letter(), radius);

		// Valid Airport
		double franc = airportService.getRequestFrequencyRatioFracForAirport(airportBOS);
		assertTrue(franc >= 1.0);
	}

	@Test
	public void test_histForRadiusFrequency() throws Exception {

		// Empty hist array
		int[] hist = airportService.histForRadiusFrequency();
		assertEquals((1000 + 1), hist.length);

		// updateRequestAndRadiusFrequencyForAirportByIataCode
		airportService.updateRequestAndRadiusFrequencyForAirportByIataCode(airportBOS.getIataFaaCode3Letter(), radius);

		hist = airportService.histForRadiusFrequency();
		assertEquals((radius.intValue() + 1), hist.length);

	}
}