package com.crossover.trial.weather.enpoints;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Set;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import com.crossover.trial.weather.endpoints.WeatherCollectorEndpoint;
import com.crossover.trial.weather.endpoints.WeatherCollectorEndpointImp;
import com.crossover.trial.weather.endpoints.WeatherQueryEndpoint;
import com.crossover.trial.weather.endpoints.WeatherQueryEndpointImp;
import com.crossover.trial.weather.models.Airport;
import com.crossover.trial.weather.models.AtmosphericInformation;
import com.crossover.trial.weather.models.DataPoint;
import com.crossover.trial.weather.models.DataPointType;
import com.crossover.trial.weather.repository.AirportRepositoryImp;
import com.crossover.trial.weather.service.AirportService;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class WeatherQueryAndCollectorEndpointsTest {

	// Endpoints
	private WeatherCollectorEndpoint _update;
	private WeatherQueryEndpoint _query;

	// Airports
	private Airport airportBOS;
	private Airport airportEWR;
	private Airport airportJFK;
	private Airport airportLGA;
	private Airport airportMMU;

	// Data Point
	private DataPoint _dp;

	// Gson
	private Gson _gson;

	// Point Type
	private String windPointType;
	private String cloudCoverPointType;

	@Before
	public void setUp() throws Exception {

		// Point Type
		cloudCoverPointType = DataPointType.CLOUDCOVER.toString();
		windPointType = DataPointType.WIND.toString();

		// Gson
		_gson = new Gson();

		// Data Point
		_dp = new DataPoint.Builder().withCount(10).withFirst(10).withMedian(20).withLast(30).withMean(22).build();

		// Airports
		airportBOS = new Airport("BOS", 42.364347, -71.005181);
		airportEWR = new Airport("EWR", 40.6925, -74.168667);
		airportJFK = new Airport("JFK", 40.639751, -73.778925);
		airportLGA = new Airport("LGA", 40.777245, -73.872608);
		airportMMU = new Airport("MMU", 40.79935, -74.4148747);

		/**
		 * AirportService loads hard coded data
		 */
		AirportService airportService = new AirportService(new AirportRepositoryImp());
		airportService.addAirportByIataCodeLatitudeLongitude(airportBOS.getIataFaaCode3Letter(),
				Double.toString(airportBOS.getLatitude()), Double.toString(airportBOS.getLongitude()));

		airportService.addAirportByIataCodeLatitudeLongitude(airportEWR.getIataFaaCode3Letter(),
				Double.toString(airportEWR.getLatitude()), Double.toString(airportEWR.getLongitude()));

		airportService.addAirportByIataCodeLatitudeLongitude(airportJFK.getIataFaaCode3Letter(),
				Double.toString(airportJFK.getLatitude()), Double.toString(airportJFK.getLongitude()));

		airportService.addAirportByIataCodeLatitudeLongitude(airportLGA.getIataFaaCode3Letter(),
				Double.toString(airportLGA.getLatitude()), Double.toString(airportLGA.getLongitude()));

		airportService.addAirportByIataCodeLatitudeLongitude(airportMMU.getIataFaaCode3Letter(),
				Double.toString(airportMMU.getLatitude()), Double.toString(airportMMU.getLongitude()));

		// WeatherQueryEndpointImp
		_query = new WeatherQueryEndpointImp(airportService);

		// WeatherCollectorEndpointImp
		_update = new WeatherCollectorEndpointImp(airportService);
	}

	@Test
	public void testCollector_ping() throws Exception {

		String responseStr = (String) _update.ping().getEntity();
		assertEquals("ready", responseStr);
	}

	@Test
	public void testCollector_updateWeather() throws Exception {

		Response response = _update.updateWeather(airportJFK.getIataFaaCode3Letter(), windPointType, _gson.toJson(_dp));
		assertEquals(Response.Status.OK.toString(), response.getStatusInfo().toString());
	}

	@Test
	public void testCollector_getAirports() throws Exception {

		Set<String> iatas = (Set<String>) _update.getAirports().getEntity();
		assertEquals(5, iatas.size());
	}

	@Test
	public void testCollector_getAirport() throws Exception {

		Airport ap = (Airport) _update.getAirport(airportJFK.getIataFaaCode3Letter()).getEntity();
		assertEquals(ap, airportJFK);
	}

	@Test
	public void testCollector_deleteAirport() throws Exception {

		Response response = _update.getAirport(airportJFK.getIataFaaCode3Letter());
		assertEquals(Response.Status.OK.toString(), response.getStatusInfo().toString());

	}

	@Test
	public void testQuery_ping() throws Exception {

		String ping = _query.ping();

		JsonElement pingResult = new JsonParser().parse(ping);
		assertEquals(0, pingResult.getAsJsonObject().get("datasize").getAsInt());
		assertEquals(0, pingResult.getAsJsonObject().get("iata_freq").getAsJsonObject().entrySet().size());

	}

	@Test
	public void testQuery_ping_for5Airports() throws Exception {

		_update.updateWeather(airportBOS.getIataFaaCode3Letter(), windPointType, _gson.toJson(_dp));
		_query.weather(airportBOS.getIataFaaCode3Letter(), "10");

		_update.updateWeather(airportEWR.getIataFaaCode3Letter(), windPointType, _gson.toJson(_dp));
		_query.weather(airportEWR.getIataFaaCode3Letter(), "50");

		_update.updateWeather(airportJFK.getIataFaaCode3Letter(), windPointType, _gson.toJson(_dp));
		_query.weather(airportBOS.getIataFaaCode3Letter(), "100");

		_update.updateWeather(airportLGA.getIataFaaCode3Letter(), windPointType, _gson.toJson(_dp));
		_query.weather(airportLGA.getIataFaaCode3Letter(), "150");

		_update.updateWeather(airportMMU.getIataFaaCode3Letter(), windPointType, _gson.toJson(_dp));
		_query.weather(airportMMU.getIataFaaCode3Letter(), "200");

		String ping = _query.ping();
		JsonElement pingResult = new JsonParser().parse(ping);
		assertEquals(5, pingResult.getAsJsonObject().get("datasize").getAsInt());
		assertEquals(5, pingResult.getAsJsonObject().get("iata_freq").getAsJsonObject().entrySet().size());

	}

	@Test
	public void testQuery_weather_for1Airpport1DataPoint() throws Exception {

		// Update "wind" DataPoint for "BOS"
		_update.updateWeather(airportBOS.getIataFaaCode3Letter(), windPointType, _gson.toJson(_dp));

		// Query "BOS" for its AtmosphericInformation
		List<AtmosphericInformation> ais = (List<AtmosphericInformation>) _query
				.weather(airportBOS.getIataFaaCode3Letter(), "0").getEntity();

		// Has "wind" DataPoint
		assertEquals(ais.get(0).findDataPointForItsTypeInAtmosphericInformation(DataPointType.WIND), _dp);
	}

	@Test
	public void testQuery_weather_for1Airpport2DataPoints() throws Exception {

		DataPoint windDp = new DataPoint.Builder().withCount(10).withFirst(10).withMedian(20).withLast(30).withMean(22)
				.build();

		// Update "wind" DataPoint for "BOS"
		_update.updateWeather(airportBOS.getIataFaaCode3Letter(), windPointType, _gson.toJson(windDp));

		DataPoint cloudCoverDp = new DataPoint.Builder().withCount(4).withFirst(10).withMedian(60).withLast(100)
				.withMean(50).build();

		// Update "cloudcover" DataPoint for "BOS"
		_update.updateWeather(airportBOS.getIataFaaCode3Letter(), cloudCoverPointType, _gson.toJson(cloudCoverDp));

		// Query "BOS" for its AtmosphericInformation
		List<AtmosphericInformation> ais = (List<AtmosphericInformation>) _query
				.weather(airportBOS.getIataFaaCode3Letter(), "0").getEntity();

		// Has "wind" DataPoint
		assertEquals(ais.get(0).findDataPointForItsTypeInAtmosphericInformation(DataPointType.WIND), windDp);

		// Has "cloudcover" DataPoint
		assertEquals(ais.get(0).findDataPointForItsTypeInAtmosphericInformation(DataPointType.CLOUDCOVER),
				cloudCoverDp);
	}

	@Test
	public void testQuery_weather_for3AirportsCheckAIForRadious() throws Exception {

		_update.updateWeather(airportJFK.getIataFaaCode3Letter(), windPointType, _gson.toJson(_dp));

		_dp = new DataPoint.Builder().withMean(40).build();
		_update.updateWeather(airportEWR.getIataFaaCode3Letter(), windPointType, _gson.toJson(_dp));

		_dp = new DataPoint.Builder().withMean(30).build();
		_update.updateWeather(airportLGA.getIataFaaCode3Letter(), windPointType, _gson.toJson(_dp));

		// check datasize response
		List<AtmosphericInformation> ais = (List<AtmosphericInformation>) _query
				.weather(airportJFK.getIataFaaCode3Letter(), "200").getEntity();
		assertEquals(3, ais.size());
	}

}