package com.crossover.trial.weather.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.crossover.trial.weather.models.Airport;
import com.crossover.trial.weather.models.AtmosphericInformation;

public class AirportRepositoryTest {

	// AirportRepository
	private AirportRepository airportRepository;

	// Airports
	private Airport airportBOS;
	private Airport airportEWR;
	private Airport airportJFK;
	private Airport airportLGA;
	private Airport airportMMU;

	@Before
	public void setUp() throws Exception {

		// AirportRepositoryImp
		airportRepository = new AirportRepositoryImp();

		// Airports
		airportBOS = new Airport("BOS", 42.364347, -71.005181);
		airportRepository.addAirport(airportBOS);

		airportEWR = new Airport("EWR", 40.6925, -74.168667);
		airportRepository.addAirport(airportEWR);

		airportJFK = new Airport("JFK", 40.639751, -73.778925);
		airportRepository.addAirport(airportJFK);

		airportLGA = new Airport("LGA", 40.777245, -73.872608);
		airportRepository.addAirport(airportLGA);

		airportMMU = new Airport("MMU", 40.79935, -74.4148747);
		airportRepository.addAirport(airportMMU);

	}

	@Test
	public void test_getIataListForAllAirports() throws Exception {

		Set<String> iatas = airportRepository.getIataListForAllAirports();

		assertEquals(5, iatas.size());
		assertTrue(iatas.contains(airportBOS.getIataFaaCode3Letter()));
		assertTrue(iatas.contains(airportEWR.getIataFaaCode3Letter()));
		assertTrue(iatas.contains(airportJFK.getIataFaaCode3Letter()));
		assertTrue(iatas.contains(airportLGA.getIataFaaCode3Letter()));
		assertTrue(iatas.contains(airportMMU.getIataFaaCode3Letter()));
	}

	@Test
	public void test_getAllAirportList() throws Exception {

		Collection<Airport> airports = airportRepository.getAllAirportList();

		assertEquals(5, airports.size());
		assertTrue(airports.contains(airportBOS));
		assertTrue(airports.contains(airportEWR));
		assertTrue(airports.contains(airportJFK));
		assertTrue(airports.contains(airportLGA));
		assertTrue(airports.contains(airportMMU));
	}

	@Test
	public void test_getAtmosphericInformationListForAllAirports() throws Exception {

		Collection<AtmosphericInformation> ais = airportRepository.getAtmosphericInformationListForAllAirports();
		assertEquals(0, ais.size());
	}

	@Test
	public void test_airportExists() throws Exception {

		// Valid Airport
		assertTrue(airportRepository.airportExists(airportBOS.getIataFaaCode3Letter()));

		// Invalid Airport
		assertFalse(airportRepository.airportExists("CRS"));
	}

	@Test
	public void test_findAirportByIataCode() throws Exception {

		// Valid Airport
		Airport ap1 = airportRepository.findAirportByIataCode(airportBOS.getIataFaaCode3Letter());
		assertEquals(ap1, airportBOS);

		// Invalid Airport
		Airport ap2 = airportRepository.findAirportByIataCode("CRS");
		assertNull(ap2);
	}

	@Test
	public void test_deleteAirport() throws Exception {

		// Delete Airport
		airportRepository.deleteAirport(airportBOS);

		Airport ap1 = airportRepository.findAirportByIataCode(airportBOS.getIataFaaCode3Letter());
		assertNull(ap1);
	}

	@Test
	public void test_addAirport() throws Exception {

		Airport newAirport = new Airport("CRS", 42.364347, -71.005181);

		// Add Airport
		airportRepository.addAirport(newAirport);

		Airport ap1 = airportRepository.findAirportByIataCode("CRS");
		assertEquals(ap1, newAirport);
	}

	@Test
	public void test_atmosphericInformationExistsForAirportIata() throws Exception {

		boolean exists = airportRepository
				.atmosphericInformationExistsForAirportIata(airportBOS.getIataFaaCode3Letter());
		assertFalse(exists);
	}

	@Test
	public void test_findAtmosphericInformationForAirportIata() throws Exception {

		AtmosphericInformation ai = airportRepository
				.findAtmosphericInformationForAirportIata(airportBOS.getIataFaaCode3Letter());
		assertNull(ai);
	}

	@Test
	public void test_updateAtmosphericInformationForAirportIata() throws Exception {

		// Update AtmosphericInformation
		airportRepository.updateAtmosphericInformationForAirportIata(airportBOS.getIataFaaCode3Letter(),
				new AtmosphericInformation());

		AtmosphericInformation ai = airportRepository
				.findAtmosphericInformationForAirportIata(airportBOS.getIataFaaCode3Letter());
		assertTrue(ai != null && ai.getDataPointMapSize() == 0);
	}

	@Test
	public void test_updateRequestFrequencyForAirportIata() throws Exception {

		// Update Frequency
		airportRepository.updateRequestFrequencyForAirportIata(airportBOS.getIataFaaCode3Letter());

		// Get Frequency
		Integer int1 = airportRepository.getRequestFrequencyForAirportIata(airportBOS.getIataFaaCode3Letter());
		assertEquals(int1, new Integer(1));

		// Get Frequency Size
		int int2 = airportRepository.getRequestFrequencySize();
		assertTrue(int2 == 1);
	}

	@Test
	public void test_getRequestFrequencyForAirportIata() throws Exception {

		// Get Req Frequency
		Integer int1 = airportRepository.getRequestFrequencyForAirportIata(airportBOS.getIataFaaCode3Letter());
		assertEquals(int1, new Integer(0));
	}

	@Test
	public void test_getRequestFrequencySize() throws Exception {

		// Get Req Frequency Size
		int int2 = airportRepository.getRequestFrequencySize();
		assertEquals(int2, 0);
	}

	@Test
	public void test_updateRadiusFrequencyForRadius() throws Exception {

		// Update Radius Frequency
		double radius = 200.0;
		airportRepository.updateRadiusFrequencyForRadius(radius);

	}

	@Test
	public void test_histForRadiusFrequency() throws Exception {

		// Empty hist array
		int[] hist = airportRepository.histArrayForRadiusFrequencyEntries();
		assertEquals((1000 + 1), hist.length);

		// Update Radius Frequency
		double radius = 200.0;
		airportRepository.updateRadiusFrequencyForRadius(radius);

		hist = airportRepository.histArrayForRadiusFrequencyEntries();
		assertEquals(((int) radius + 1), hist.length);

	}
}